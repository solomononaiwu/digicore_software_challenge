1. Clone project using :    `git clone https://gitlab.com/solomononaiwu/digicore_software_challenge.git`
2. Open project in appropriate IDE i.e Netbeans
3. Clean and build project
4. If Any Errors on build - Fix by downloading project dependecies
5. Run Project
6. Once project is running you can check on http://localhost:8080/swagger-ui.html in your browser to view the swagger UI and test the endpoints.
