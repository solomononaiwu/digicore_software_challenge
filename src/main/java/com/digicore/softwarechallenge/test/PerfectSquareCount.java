package com.digicore.softwarechallenge.test;

import java.util.Scanner;

/**
 *
 * @author Solomon onaiwu
 */
public class PerfectSquareCount {

    static int countPerfectSquares(int l, int b) {
        
        if (b < l) {
            int temp = l;
            l = b;
            b = temp;
        }
        return l * (l + 1) * (2 * l + 1)
                / 6 + (b - l) * l * (l + 1) / 2;
    }

    public static void main(String[] args) {
        
        int l = 1, b = 0;
        System.out.println("|******************************************************************|");
        System.out.println(" Sample Input 1");
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ "  grid is = "+ countPerfectSquares(l, b));
        l=1; b = 1;
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ "  grid is = "+ countPerfectSquares(l, b));
        System.out.println("|******************************************************************|");
        
        System.out.println("|******************************************************************|");
        System.out.println(" Sample Input 2");
        l=2; b=0;
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ "  grid is = "+ countPerfectSquares(l, b));
        l=3; b=3;
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ "  grid is = "+ countPerfectSquares(l, b));
         l=4; b=5;
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ "  grid is = "+ countPerfectSquares(l, b));
        System.out.println("|******************************************************************|");
        
        
        
        System.out.println("USER INPUT");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Grid Length: L =");
        l = scanner.nextInt();
        System.out.print("Enter Grid Width: W =");
        b = scanner.nextInt();
        
        System.out.println("|******************************************************************|");
        System.out.println("The Count of perfect squares in a "  +l+"X"+b+ " grid is = "+ countPerfectSquares(l, b));
        System.out.println("|******************************************************************|");
    }
}
