/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digicore.softwarechallenge.repository;

import com.digicore.softwarechallenge.model.Account;
import com.digicore.softwarechallenge.model.AccountStatement;
import java.util.ArrayList;
import java.util.HashSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Onaiwu solomon
 */
@Repository
public class AccountRepository {

    HashSet<Account> accountSet = new HashSet<Account>();

    public AccountRepository() {
        //default users || Kindly note this is for testing purpose
        Account a1 = new Account("Onaiwu Solomon", "0266428405", 500000.00, "$2y$12$pSxU5x8J4yN2fIp1uKpU9.I.yXMsn04Gp6ujp9VG2wad2QtrMlF/2");//password is 1234
        ArrayList<AccountStatement> aStatement = new ArrayList<>();
        a1.setAccountStatement(aStatement);
        accountSet.add(a1);
    
    }

    public HashSet<Account> getAllAcounts() {
        return accountSet;
    }

    public void addAccount(Account account) {
        accountSet.add(account);
    }

    public void removeAccount(Account account) {
        accountSet.remove(account);
    }

}
