package com.digicore.softwarechallenge.controller;

import com.digicore.softwarechallenge.dto.AccountInfoInputDTO;
import com.digicore.softwarechallenge.dto.CreateAccountInputDTO;
import com.digicore.softwarechallenge.response.DefaultResponses;
import com.digicore.softwarechallenge.service.AccountService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Onaiwu solomon
 *
 * This Controller handles all issues relating to user account
 */
@RestController
@CrossOrigin
public class AccountController {

    private static final Logger L = LogManager.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    //this was added in order to see the account numbers generated for each new account when the create_account endpoint is called
    @GetMapping("account")
    public ResponseEntity getAccounts() {
        try {
            return accountService.getAllAccounts();
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Getting Accounts , Error Case: " + e.getClass().getName()));
        }

    }

    @PostMapping("account_info")
    public ResponseEntity getAccountDetail(@RequestBody AccountInfoInputDTO accountInfoInputDTO) {
        try {
            return accountService.getAccountDetail(accountInfoInputDTO);
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Getting Account Details, Error Case: " + e.getClass().getName()));
        }

    }

    @PostMapping("account_statement")
    public ResponseEntity getAccountStatement(@RequestBody AccountInfoInputDTO accountInfoInputDTO) {
        try {
            return ResponseEntity.ok(accountService.getAccountStatement(accountInfoInputDTO));
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Getting Account Statement, Error Case: " + e.getClass().getName()));
        }

    }

    @PostMapping("create_account")
    public ResponseEntity createAccount(@RequestBody CreateAccountInputDTO account) {
        try {
            return accountService.createAccount(account);
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Createing account, Error Case: " + e.getClass().getName()));
        }

    }
}
