package com.digicore.softwarechallenge.controller;

import com.digicore.softwarechallenge.dto.DepositInputDTO;
import com.digicore.softwarechallenge.dto.WithdrawalInputDTO;
import com.digicore.softwarechallenge.response.DefaultResponses;
import com.digicore.softwarechallenge.service.TransactionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Onaiwu solomon
 *
 * This Controller handles all issues relating to transactions such as deposit
 * and withdrawals.
 */
@RestController
@CrossOrigin
public class TransactionController {

    private static final Logger L = LogManager.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    @PostMapping("deposit")
    public ResponseEntity deposit(@RequestBody DepositInputDTO depositInputDTO) {
        try {
            return transactionService.deposit(depositInputDTO);
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Depositing, Error Case: " + e.getClass().getName()));
        }

    }
    
    @PostMapping("withdrawal")
    public ResponseEntity withdrawal(@RequestBody WithdrawalInputDTO withdrawalInputDTO) {
        try {
            return transactionService.withdraw(withdrawalInputDTO);
        } catch (Exception e) {
            L.error("Error during Processing", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(DefaultResponses.response500("Error Withdrawing, Error Case: " + e.getClass().getName()));
        }

    }
}
