/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digicore.softwarechallenge.enums;

/**
 *
 * @author Onaiwu solomon
 */
public enum TransactionType {
    Deposit, Withdrawal
}
