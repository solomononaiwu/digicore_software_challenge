package com.digicore.softwarechallenge.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Onaiwu solomon
 */
@Service
public class UtilService {

    private static final long LIMIT = 10000000000L;
    private static long last = 0;
    
    @Autowired
    BCryptPasswordEncoder encoder ;

    public long generateAccountNumber() {
        long id = System.currentTimeMillis() % LIMIT;
        if (id <= last) {
            id = (last + 1) % LIMIT;
        }
        return last = id;
    }

   
    public String hashPassword(String plainTextPassword) {
        String encodedPassword = encoder.encode(plainTextPassword);
        return encodedPassword;
    }

  
    public boolean checkPassword(String plainTextPassword, String hashedPassword) {
        return encoder.matches(plainTextPassword, hashedPassword);
    }
}
