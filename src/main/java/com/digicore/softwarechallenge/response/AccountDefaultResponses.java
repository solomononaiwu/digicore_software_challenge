
package com.digicore.softwarechallenge.response;

import org.springframework.http.HttpStatus;

/**
 *
 * @author Onaiwu solomon
 */
public class AccountDefaultResponses {
      public static <E> AccountApiResponse response200(String message, E object) {
        return new AccountApiResponse( HttpStatus.OK.value(),true, message, object);
    }

   

    public static AccountApiResponse response200(String message) {
        return new AccountApiResponse(true, HttpStatus.OK.value(), message);
    }
    public static AccountApiResponse response400(String message) {
        return new AccountApiResponse(false, HttpStatus.BAD_REQUEST.value(), message);
    }
    
      public static AccountApiResponse response401(String message) {
        return new AccountApiResponse(false, HttpStatus.UNAUTHORIZED.value(), message);
    }

    public static AccountApiResponse response500(String message) {
        return new AccountApiResponse(false, HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

}
