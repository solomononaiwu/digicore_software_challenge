
package com.digicore.softwarechallenge.response;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author Onaiwu solomon
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountApiResponse {

    private int responseCode;
    private boolean success;
    private String message;
    private Object account; //optional

    public AccountApiResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public AccountApiResponse(boolean success, String message, Object account) {
        this.success = success;
        this.message = message;
        this.account = account;
    }

    public AccountApiResponse(boolean success, int responseCode, String message) {
        this.success = success;
        this.responseCode = responseCode;
        this.message = message;
    }

    public AccountApiResponse(int responseCode, boolean success, String message, Object account) {
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
        this.account = account;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getAccount() {
        return account;
    }

    public void setAccount(Object account) {
        this.account = account;
    }
    
    
    

}
