package com.digicore.softwarechallenge.response;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Solomon onaiwu
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {
    private int responseCode;
    private boolean successful;
    private String message;
    private Object object; //optional

    public ApiResponse(boolean success, String message) {
        this.successful = success;
        this.message = message;
    }
    
    

    public ApiResponse(boolean success, String message, Object object) {
        this.successful = success;
        this.message = message;
        this.object = object;
    }



    public ApiResponse(boolean success, int responseCode,String message) {
        this.successful = success;
        this.responseCode = responseCode;
        this.message = message;
    }
    

    public ApiResponse(int responseCode, boolean success, String message, Object object) {
        this.responseCode = responseCode;
        this.successful = success;
        this.message = message;
        this.object = object;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

   

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
    
    


}
