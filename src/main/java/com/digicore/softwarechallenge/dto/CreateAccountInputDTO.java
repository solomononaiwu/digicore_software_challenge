package com.digicore.softwarechallenge.dto;

/**
 *
 * @author Onaiwu solomon
 */
public class CreateAccountInputDTO {
    private String accountName;
    private String accountPassword;

    public CreateAccountInputDTO() {
    }

    public CreateAccountInputDTO(String accountName, String accountPassword) {
        this.accountName = accountName;
        this.accountPassword = accountPassword;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }
    
}
