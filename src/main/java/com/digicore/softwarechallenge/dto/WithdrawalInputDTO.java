/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digicore.softwarechallenge.dto;

/**
 *
 * @author Onaiwu solomon
 */
public class WithdrawalInputDTO {

   private String accountNumber;
   private String accountPassword;
   private Double withdrawnAmount;

    public WithdrawalInputDTO() {
    }

    public WithdrawalInputDTO(String accountNumber, String accountPassword, Double withdrawnAmount) {
        this.accountNumber = accountNumber;
        this.accountPassword = accountPassword;
        this.withdrawnAmount = withdrawnAmount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public Double getWithdrawnAmount() {
        return withdrawnAmount;
    }

    public void setWithdrawnAmount(Double withdrawnAmount) {
        this.withdrawnAmount = withdrawnAmount;
    }
   
   
}
