/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digicore.softwarechallenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;

/**
 *
 * @author Onaiwu solomon
 */
public class Account {
    private String accountName;
    private String accountNumber;
    private Double balance;
    @JsonIgnore
    private String accountPassword;
    private ArrayList<AccountStatement> accountStatement;

    public Account() {
    }

    public Account(String accountName, String accountNumber, Double balance, String accountPassword) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.accountPassword = accountPassword;
    }

    public Account(String accountName, String accountNumber, Double balance, String accountPassword, ArrayList<AccountStatement> accountStatement) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.accountPassword = accountPassword;
        this.accountStatement = accountStatement;
    }
    
    

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public ArrayList<AccountStatement> getAccountStatement() {
        return accountStatement;
    }

    public void setAccountStatement(ArrayList<AccountStatement> accountStatement) {
        this.accountStatement = accountStatement;
    }
    
    

    @Override
    public String toString() {
        return "Account{" + "accountName=" + accountName + ", accountNumber=" + accountNumber + ", balance=" + balance + '}';
    }
    
    
    
}
