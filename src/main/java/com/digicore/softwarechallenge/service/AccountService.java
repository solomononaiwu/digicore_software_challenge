package com.digicore.softwarechallenge.service;

import com.digicore.softwarechallenge.model.Account;
import com.digicore.softwarechallenge.dto.AccountInfoInputDTO;
import com.digicore.softwarechallenge.dto.AccountInfoOutputDTO;
import com.digicore.softwarechallenge.dto.CreateAccountInputDTO;
import com.digicore.softwarechallenge.model.AccountStatement;
import com.digicore.softwarechallenge.repository.AccountRepository;
import com.digicore.softwarechallenge.response.AccountDefaultResponses;
import com.digicore.softwarechallenge.utils.UtilService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Onaiwu solomon
 */
@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UtilService utilService;

    public ResponseEntity getAccountDetail(AccountInfoInputDTO accountInfoInputDTO) {

        for (Account account : accountRepository.getAllAcounts()) {
            if (account.getAccountNumber().equals(accountInfoInputDTO.getAccountNumber())
                    && utilService.checkPassword(accountInfoInputDTO.getAccountPassword(), account.getAccountPassword())) {
                AccountInfoOutputDTO accountInfo = new AccountInfoOutputDTO();
                accountInfo.setAccountName(account.getAccountName());
                accountInfo.setAccountNumber(account.getAccountNumber());
                accountInfo.setBalance(account.getBalance());
                return ResponseEntity.status(HttpStatus.OK).body(AccountDefaultResponses.response200("Successfully fetched account", accountInfo));

            }
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(AccountDefaultResponses.response401("Incorrect account number or password"));
    }

    public ArrayList<AccountStatement> getAccountStatement(AccountInfoInputDTO accountInfoInputDTO) {
      
        ArrayList<AccountStatement> accountStatement = new ArrayList<>();
        for (Account account : accountRepository.getAllAcounts()) {
            if (account.getAccountNumber().equals(accountInfoInputDTO.getAccountNumber())
                    && utilService.checkPassword(accountInfoInputDTO.getAccountPassword(), account.getAccountPassword()) ) {

                accountStatement = account.getAccountStatement();
                return accountStatement;
            }
        }
        return accountStatement;
    }

    public ResponseEntity createAccount(CreateAccountInputDTO account) {

        for (Account userAccount : accountRepository.getAllAcounts()) {
            if (userAccount.getAccountName().equals(account.getAccountName())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(AccountDefaultResponses.response400("An account already exist with same accountName"));
            }
        }

        Account newAccount = new Account();
        newAccount.setAccountName(account.getAccountName());
        newAccount.setAccountPassword(utilService.hashPassword(account.getAccountPassword()));
        newAccount.setBalance(0.0);
        newAccount.setAccountStatement(new ArrayList<>());
        newAccount.setAccountNumber(utilService.generateAccountNumber() + "");

        accountRepository.addAccount(newAccount);
        return ResponseEntity.status(HttpStatus.OK).body(AccountDefaultResponses.response200("Account created successfully"));

    }

    public ResponseEntity getAllAccounts() {
        return ResponseEntity.status(HttpStatus.OK).body(AccountDefaultResponses.response200("Successfully fetched all accounts", accountRepository.getAllAcounts()));

    }
}
