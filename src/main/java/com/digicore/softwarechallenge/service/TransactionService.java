package com.digicore.softwarechallenge.service;

import com.digicore.softwarechallenge.dto.DepositInputDTO;
import com.digicore.softwarechallenge.dto.WithdrawalInputDTO;
import com.digicore.softwarechallenge.enums.TransactionType;
import com.digicore.softwarechallenge.model.Account;
import com.digicore.softwarechallenge.model.AccountStatement;
import com.digicore.softwarechallenge.repository.AccountRepository;
import com.digicore.softwarechallenge.response.DefaultResponses;
import com.digicore.softwarechallenge.utils.UtilService;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Onaiwu solomon
 */
@Service
public class TransactionService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UtilService utilService;

    public ResponseEntity deposit(DepositInputDTO depositInputDTO) {

        if (depositInputDTO.getAmount() > 1000000 || depositInputDTO.getAmount() < 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(DefaultResponses.response400("Deposit Transaction Rejected : Amount must be >=1 or <=1000000"));
        }

        for (Account account : accountRepository.getAllAcounts()) {
            if (account.getAccountNumber().equals(depositInputDTO.getAccountNumber())) {
                AccountStatement statement = new AccountStatement(new Date(), TransactionType.Deposit.name(), "Cash deposit", depositInputDTO.getAmount(), account.getBalance() + depositInputDTO.getAmount());
                account.setBalance(account.getBalance() + depositInputDTO.getAmount());
                ArrayList<AccountStatement> accountStatement = new ArrayList<>(account.getAccountStatement());
                accountStatement.add(statement);
                account.setAccountStatement(accountStatement);
                accountRepository.addAccount(account);
                return ResponseEntity.status(HttpStatus.OK).body(DefaultResponses.response200("Deposit Successful"));
            }
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(DefaultResponses.response400("Account Does not Exist"));

    }

    public ResponseEntity withdraw(WithdrawalInputDTO withdrawalInputDTO) {

        if (withdrawalInputDTO.getWithdrawnAmount() < 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(DefaultResponses.response400("Withdrawal Failed: Amount Withdrawn must be >=1"));
        }

        for (Account account : accountRepository.getAllAcounts()) {
            if (account.getAccountNumber().equals(withdrawalInputDTO.getAccountNumber())
                    && utilService.checkPassword(withdrawalInputDTO.getAccountPassword(), account.getAccountPassword())) {
                if ((account.getBalance() - withdrawalInputDTO.getWithdrawnAmount()) < 500) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(DefaultResponses.response400("Insufficient Fund - You must have a minimum of N500 in your account"));
                }
                AccountStatement statement = new AccountStatement(new Date(), TransactionType.Withdrawal.name(), "Cash withdrawal",
                        withdrawalInputDTO.getWithdrawnAmount(),
                        account.getBalance() - withdrawalInputDTO.getWithdrawnAmount());

                account.setBalance(account.getBalance() - withdrawalInputDTO.getWithdrawnAmount());
                ArrayList<AccountStatement> accountStatement = new ArrayList<>(account.getAccountStatement());
                accountStatement.add(statement);
                account.setAccountStatement(accountStatement);
                accountRepository.addAccount(account);
                return ResponseEntity.status(HttpStatus.OK).body(DefaultResponses.response200("Withdrawal Successful"));
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(DefaultResponses.response400("Account Does not Exist"));
    }
}
