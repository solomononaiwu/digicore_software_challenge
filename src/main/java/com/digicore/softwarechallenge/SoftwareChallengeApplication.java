package com.digicore.softwarechallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwareChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftwareChallengeApplication.class, args);
	}

}
